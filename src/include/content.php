

    <div class="jumbotron">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <h1 class="display-3">Car <span class="badge badge-info">Mechanic</span></h1>
            <h3>Not just a job!</h3>
          </div>
          <div class="col-lg-6">
            <img class="img-fluid showcase-img" src="img/jumbo_tools.png">
          </div>
        </div>
      </div>
    </div>

    <section id="middle">
       <!--Creates the popup body-->
       <div class="popup-overlay">
        <!--Creates the popup content-->
        <div class="popup-content">
            <h2>Congratulations, DOOM Fan!</h2>
            <p><img class="img-fluid" src="img/revenant.gif"></p>
            <p>Send us an e-mail with "John Romero Rules" in the subject field, and you will receive a prize when you visit us!</p>
          <!--popup's close button-->
            <button class="btn btn-outline-secondary closeMe">Close</button>    
        </div>
        </div>
      <div class="container" id="pageContent">
      
         <?php
         echo <<<EOT

         <div class="card-body">
         <h2 class="card-title">Car Expertise</h2>
         <p class="card-text">We have repaired thousands of cars, and we can proudly say that our mechanics can fix any problem. It doesn't matter whether 
         you need vintage car repairs, luxury car repairs, or sport car repairs; what matters is that we can do it!</p>

       </div>
        <div class="row">
          <div class="col-lg-4">
            <picture class="car-photo img-fluid">
              <source srcset="img/vintage_car_medium.jpg" media="(min-width: 1200px)">
              <source srcset="img/vintage_car_small.jpg" media="(min-width: 1000px)">
              <source srcset="img/vintage_car_medium-large.jpg" media="(max-width: 768px)">
              <img class="img-fluid" srcset="img/vintage_car_large.jpg" media="(max-width: 800px)">
            </picture>
           
            <h3>Vintage Cars</h3>
            <p>We have done plenty of work on various vintage cars, so there is no need to worry whether we can solve a problem with your precious vintage car.
            Our mechanics have managed to repair many vintage cars, and the best part is that we are capable of finding those rare parts for you!
            </p>
          
          </div>
          <div class="col-lg-4">
            <picture class="car-photo img-fluid">
              <source srcset="img/sport_car_medium.jpg" media="(min-width: 1200px)">
              <source srcset="img/sport_car_small.jpg" media="(min-width: 1000px)">
              <source srcset="img/sport_car_medium-large.jpg" media="(max-width: 768px)">
              <img class="img-fluid" srcset="img/sport_car_large.jpg" media="(max-width: 800px)">
            </picture>
            <h3>Sport Cars</h3>
            <p>We love sport cars, a few of us own them, and we love repairing them. Our communication with parts importers are a guarantee that you won't be able to find 
            parts for your car at a lower price anywhere else - that is a guarantee!
            </p>
           
          </div>
          <div class="col-lg-4">
            <picture class="car-photo img-fluid">
              <source srcset="img/luxury_car_medium.jpeg" media="(min-width: 1200px)">
              <source srcset="img/luxury_car_small.jpg" media="(min-width: 1000px)">
              <source srcset="img/luxury_car_medium-large.jpeg" media="(max-width: 768px)">
              <img class="img-fluid" srcset="img/luxury_car_large.jpeg" media="(max-width: 800px)">
            </picture>
            <h3>Luxury Cars</h3>
            <p>We are aware of the fact that your luxury car is extremely important to you, so we promise that we will take extra-special care of it! You can rest assured that, 
            when you bring in your luxury car for a fixup, we will make sure that we will return it to you looking better than it did comming in!
            </p>
         
          </div>
        </div>
        
EOT;
?>
        
      
    </section>

    <section id="feature">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h1>What We Offer</h1>
            <ul>
              <li><i class="fa fa-check" aria-hidden="true"></i>High-Quality Parts</li>
              <li><i class="fa fa-check" aria-hidden="true"></i>Fastest Service</li>
              <li><i class="fa fa-check" aria-hidden="true"></i>Decades of Experience</li>
              <li><i class="fa fa-check" aria-hidden="true"></i>Best Prices</li>
            </ul>
          </div>
          <div class="col-md-6">
            <img id="big-logo" src="img/logo.png">
          </div>
          
        </div>
      </div>
    </section>