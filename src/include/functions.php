<?php
function get_user_name($user_id) {

    global $connection;
    $sql = "SELECT CONCAT(firstname,' ', lastname) AS name
		FROM users
		WHERE user_id='$user_id'";
    $result = mysqli_query($connection, $sql) or die(mysqli_error($connection));

    if (mysqli_num_rows($result) > 0) {
        while ($record = mysqli_fetch_array($result, MYSQLI_BOTH)) {
            $name = $record['name'];
        }
    }

    return $name;
}

function insert_problem($user_id, $user_comment, $connection) {

    $sql_ins = "INSERT INTO problems(user_id,problem_date,user_comment )
VALUES ('$user_id',NOW(),'$user_comment')";
    if (mysqli_query($connection, $sql_ins)) {
        $problem_id = mysqli_insert_id($connection);
    }
    return $problem_id;
};

function insert_car($problem_id, $plate_num, $brand, $model, $car_year, $connection) {

    $sql_ins_res="INSERT INTO problem_reservation(problem_id, reservation_date, reservation_start, reservation_end) VALUES ('$problem_id', CURDATE(),'00:00:00', '00:00:00')";

    mysqli_query($connection, $sql_ins_res);

    $sql_ins = "INSERT INTO cars(problem_id, plate_num, car_brand, car_model, car_year) VALUES ('$problem_id','$plate_num','$brand','$model','$car_year')";
    if (mysqli_query($connection, $sql_ins)) {
        $car_id = mysqli_insert_id($connection);
    }
    return $car_id;
};

function get_time_price($problems, $problem_id, $connection) {
    $sql = "SELECT service_id, service_time, service_price FROM services WHERE service_id IN ($problems)";

    $result = mysqli_query($connection, $sql) or die(mysql_error($connection));

    if ($result->num_rows > 0) {
        $problem_time_temp = "00:00:00";
        $problem_time_temp = strtotime($problem_time_temp);
        $service_price_temp = 0;
        while ($row = $result->fetch_assoc()) {

            $service_id = $row['service_id'];
            $service_time = $row['service_time'];
            $service_price = $row['service_price'];

            $sql_ins = "INSERT INTO problem_service(problem_id, service_id, problem_service_price, problem_service_time) VALUES ('$problem_id','$service_id','$service_price','$service_time')";
            mysqli_query($connection, $sql_ins);

            $problem_time_temp = $problem_time_temp + strtotime($service_time) - strtotime("00:00:00");
            $service_price_temp = $service_price_temp + $service_price;
        }
    }
    return array(
        $problem_time_temp,
        $service_price_temp
    );
};

function format_time($problem_time_temp) {
    $problem_time_temp1 = date('H:i:s', $problem_time_temp);
    function decimal($time) {
        $hms = explode(":", $time);
        return ($hms[0] + ($hms[1] / 60) + ($hms[2] / 3600));
    }
    //rounding number if 1.0 it will be 1, from.01 to .5 it will be 0.5, and 0.51 to .99 it will be 1
    $problem_time_fl = round((decimal($problem_time_temp1) * 2) + 0.49999) / 2;
    //rounding up the time to half hour interval
    $problem_time_rounded = date('H:i:s', ceil(strtotime($problem_time_temp1) / 1800) * 1800);
    return array(
        $problem_time_fl,
        $problem_time_rounded,
        $problem_time_temp1
    );
}

function sendEmail($email, $name, $message) {
    require "vendor/autoload.php";
    //Create a new PHPMailer instance
    $mail = new PHPMailer\PHPMailer\PHPMailer();
    //Tell PHPMailer to use SMTP - requires a local mail server
    //Faster and safer than using mail()
    $mail->isSMTP();
    $mail->Host = 'localhost';
    // $mail->Host = "ssl://smtp.gmail.com";
    $mail->Port = 25;
    //Use a fixed address in your own domain as the from address
    //**DO NOT** use the submitter's address here as it will be forgery
    //and will cause your messages to fail SPF checks
    $mail->setFrom('damir-gile@nashserver.com', 'Damir i Gile project');
    //Send the message to yourself, or whoever should receive contact for submissions
    $mail->addAddress('damir-gile@nashserver.com', 'Damir i Gile project');
    //Put the submitter's address in a reply-to header
    //This will fail if the address provided is invalid,
    //in which case we should ignore the whole request
    if ($mail->addReplyTo($email, $name)) {
        $mail->Subject = 'PHPMailer contact form';
        //Keep it simple - don't use HTML
        $mail->isHTML(true);
        //Build a simple message body
        $mail->Body = $message;
        //Send the message, check for errors
        if (!$mail->send()) {
            return false;
            //The reason for failing to send will be in $mail->ErrorInfo
            //but you shouldn't display errors to users - process the error, log it on your server.
            
        }
        else {
            return true;
        }
    }
}

