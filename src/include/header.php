<?php

session_start();

require("config.php");
require("db.php");
require("functions.php");
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/font-awesome.css">
    <meta name="description" content="The best car workshop with the lowest prices! High-quality service and low-price car parts.">
    <meta name="keywords" content="car repair, car workshop, car tools, car repairman, fix my car, cheap car service, cheap car fix, cheap car parts">
    <meta name="author" content="Damir & Gile">
    <link rel="icon" 
      type="image/png" 
      href="myicon.png" />
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <!-- DataTable CSS -->
    <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedcolumns/3.2.6/css/fixedColumns.dataTables.min.css">
   <!-- google captcha -->
    <script type="text/javascript">
      var onloadCallback = function() {
        grecaptcha.render('captcha_element', {
          'sitekey' : '6LecRWQUAAAAADG7HkkApM3fVC_h8rV8J3kdgdX5'
        });
      };
    </script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script  type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script  type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script  type="text/javascript" src="node_modules\jquery-validation\dist\jquery.validate.js"></script>
    <!-- Data TAbel js -->
    <script type="text/javascript" src="js/jquery.redirect.js"></script>
    <script type="text/javascript" charset="utf8" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"></script>
    <!-- END DataTable js -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7/dist/sweetalert2.all.min.js"></script>
    <title>Car Workshop</title>
  </head>

<body>
<nav class="navbar navbar-expand-md navbar-light fixed-top justify-content-center py-md-0">
    <div class="container" id="navi">
      <a class="navbar-brand em-text" href="index.php">
        <img src="img/logo.png" width="30" height="30" alt="">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarLinks" aria-controls="#navbarLinks" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarLinks">
        <ul class="nav navbar-nav mr-auto" id="links">
            <li><a href="#" class="nav-link">Home</a></li>
            <li><a href="#pageservices.php" class="nav-link">Services</a></li>
            <li><a href="#pageourteam.php" class="nav-link">Our Team</a></li>
            <li><a href="#pageaboutus.php" class="nav-link">About Us</a></li>
            <li><a href="#pagecontact.php" class="nav-link">Contact Us</a></li>
        </ul>

        <ul class="nav navbar-nav navbar-right flex-row justify-content-start ml-auto">
          <?php
          if(isset($_SESSION['user_id'])) {
            $name = get_user_name($_SESSION['user_id']);
              
            echo "<li><a href=\"logout.php\" class=\"nav-link\">Logout</a></li>";
              		
          }else { 
            echo <<<EOT
            <li class="dropdown order-1">
              <button type="button" id="dropdownMenu1" data-toggle="dropdown" class="btn btn-outline-secondary dropdown-toggle navi-hider">Login <span class="caret"></span></button>
              <ul class="dropdown-menu dropdown-menu-right mt-2 dropdown-form">
                <li class="px-3 py-2">
                  <!--on click/forget change content in form-->
                  <form class="form formvalidate drop-mobile" id="formlogin" action="login.php" method="post" autocomplete="off" role="form">
                    <div class="form-group">
                      <label for="email">Email</label>
                      <input type="email" name="email" id="email1" placeholder="Email" class="form-control form-control-sm">
                    </div>
                    <div class="form-group">
                      <label for="password">Password</label>
                      <input type="password" name="password" id="password1" placeholder="Password" class="form-control form-control-sm">
                    </div>
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary btn-block" name="login" >Login</button>
                    </div>
                    <button type="reset" class="btn btn-primary btn-block" name="reset" value="cancel">RESET</button>
                    <div class="form-group text-center">
                      <small><a href="#pageregister.php" >Register</a></small>
                    </div>
                    <div class="form-group text-center">
                      <small><p class="openit_forgot">Forgot password?</p></small>
                    </div>
                  </form>
                  <!--on click/forget change content in form-->
                  <form class="form formvalidate formforget remove" id="formforget" role="form" action="forget.php" method="post" autocomplete="off">
                    <div class="form-group">
                      <label for="email">Email</label>
                      <input id="email2" placeholder="Email" class="form-control form-control-sm" type="email" name="email">
                    </div>
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary btn-block" name="forget">Reset Password</button>
                    </div>
                    <button type="reset" class="btn btn-primary btn-block" name="reset" value="cancel">RESET</button>
                  </form>
                </li>
              </ul>
            </li>

EOT;
                          
          }                               
          ?>
        </ul>
      </div>
    </div>
  </nav>

  <?php	
  //if the role_id is 2, then the user doesn't have admin privileges
  if(isset($_SESSION['user_id']) && isset($_SESSION['role_id']) && $_SESSION['role_id'] == 2) {
    
  echo <<<EOT
  
  <nav class="navbar fixed-top navbar-light navbar-left bg-light justify-content-center" id="navi2">
    <div class="container navi2-container">
      <ul class="nav mr-auto">
        <li>
          <p class="navbar-text navbar-left"><strong>Welcome $name</strong></p>
        </li>

        <li  class="dropdown">
          <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Choose a Service<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li>
              <a class="nav-link" href="index.php?#pagereportproblem.php">Submit Vehicle</a>
            </li>
            <li>
              <a class="nav-link" href="index.php?#pagecheck_Status.php">Check Status</a>
            </li>
          </ul>
        </li>
      </ul>
      
     </div>
  </nav>

EOT;
  }
  //if the role_id is 1, then the user has admin privileges
  elseif(isset($_SESSION['user_id']) && isset($_SESSION['role_id']) && $_SESSION['role_id'] == 1){
   
    echo <<<EOT
    <nav class="navbar fixed-top navbar-light navbar-left bg-light justify-content-center" id="navi2">
      <div class="container navi2-container">
        <ul class="nav mr-auto">
          <li>
            <p class="navbar-text navbar-left"><strong> Welcome $name </strong></p>
          </li>

          <li class="dropdown">
            <a  id="nohref" class="dropdown-toggle nav-link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Choose a Service<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li>
                <a class="nav-link" href="#pageadminservices.php">Edit Services</a>
              </li>
              <li>
                <a class="nav-link" href="admin/jsonfordatatable.php?problem_status_id=5">Check ALL problems</a>
              </li>
            </ul>
          </li>
EOT;
                 
if (isset($_SESSION['new_problems'])) {
  $count = $_SESSION['new_problems'];
  echo <<<EOT
          <li>
            <p class="nav-link animated infinite flash" id="newproblems" href="#">New Problems: <span class="badge badge-primary">$count</span></p>
          </li>
        </ul>
      </div>
    </nav>
EOT;
}
else {
  echo <<<EOT
          <li>
            <a class="nav-link">There are NO NEW problems!</a>
          </li>
        </ul>
      </div>
    </nav>
EOT;
}
  }
  ?>
<script>

$('#newproblems').on('click', function(e) {
  location.href = "admin/jsonfordatatable.php?problem_status_id=1";
});

$('a[href]').on('click', function(e) {
  $('.jumbotron').addClass('show');
});
</script>