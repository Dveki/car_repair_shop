
<?php
require("include/config.php");
require("include/db.php");
require("include/gump.class.php");

if(isset($_POST["s_rowid"]) && isset($_POST["s_name"]) && isset($_POST["s_desc"]) && isset($_POST["s_time"]) && isset($_POST["s_price"])){

$validator = new GUMP();

$service_id = mysqli_real_escape_string($connection, $_POST["s_rowid"]);
$service_name = mysqli_real_escape_string($connection, $_POST["s_name"]);
$service_description = mysqli_real_escape_string($connection, $_POST["s_desc"]);
$service_time = mysqli_real_escape_string($connection, $_POST["s_time"]);
$service_price = mysqli_real_escape_string($connection, $_POST["s_price"]);

$_POST = array(
  'serviceid'   => $service_id,
  'servicename' 	  => $service_name,
  'servicedesc'	    => $service_description,
  'servicetime'     => $service_time,
  'serviceprice'    => $service_price
);

$_POST = $validator->sanitize($_POST);

$rules = array(
  'serviceid'   => 'required',
  'servicename' 	  => 'alpha_space',
  'servicedesc'	    => 'min_len,1',
  'servicetime'     => 'min_len,8',
  'serviceprice'    => 'numeric'
);

$filters = array(
  'serviceid'   => 'trim|sanitize_string',
  'servicename' 	  => 'trim|sanitize_string',
  'servicedesc'	    => 'trim|sanitize_string',
  'servicetime'     => 'trim|sanitize_string',
  'serviceprice'    => 'trim|sanitize_string'
);

$_POST = $validator->filter($_POST, $filters);


$validated = $validator->validate(
    $_POST, $rules
);


if($validated === TRUE){

if(isset($_FILES["file"]) AND is_uploaded_file($_FILES['file']['tmp_name'])) {

      $file_name = $_FILES['file']["name"];
      $file_temp = $_FILES["file"]["tmp_name"];
      $file_size = $_FILES["file"]["size"];
      $file_type = $_FILES["file"]["type"];
      $file_error = $_FILES['file']["error"];
    
      if ($file_error >0) {
        echo "Something went wrong during file upload!";
      }
      else {
    
        if (!exif_imagetype($file_temp)) {
          exit("File is not a picture!");
        }
    
        $ext_temp = explode(".", $file_name);
        $extension = end($ext_temp);
    
        $new_file_name = $ext_temp[0] . ".$extension";
        // 20171110084338.jpg
        $directory = "img/services";
    
        $upload = "$directory/$new_file_name"; // images/20171110084338.jpg
    
        // upload fajla
        if (!is_dir($directory)) {
          mkdir($directory);
        }
    
    
        if (move_uploaded_file($file_temp, $upload)) {
    
        }
        else {
          echo "<p><b>Error!</b></p>";
        }
    
      }
    
    }
    
    if(isset($new_file_name)) {
      $sql = "UPDATE services SET service_name='$service_name', service_description ='$service_description', service_time= '$service_time', service_price ='$service_price',service_image= '$new_file_name' WHERE service_id ='$service_id'";
    }
    else{
$sql = "UPDATE services SET service_name='$service_name', service_description ='$service_description', service_time= '$service_time', service_price ='$service_price' WHERE service_id ='$service_id'";
    }
$result = mysqli_query($connection,$sql) or die(mysqli_error($connection));

if(mysqli_query($connection, $sql)){
  echo "You have successfully edited a service.";
  exit();
}
else{
  echo "Error";
  exit();
}

}
else{
  // echo "ispod je _post <br>";
  // print_r($_POST); 
  // echo "<br>";
  // echo "ispod su validated <br>";
  // print_r($validated); // Shows all the rules that failed along with the data
  // echo "<br>";
echo $validator->get_readable_errors(true);
}
}
else{
  header("location: index.php");
  exit();
}
?>


