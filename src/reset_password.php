<?php
/* Password reset process, updates database with new user password */
session_start();
require("include/config.php");
require("include/db.php");
require("include/gump.class.php");

// Make sure the form is being submitted with method="post"

if (!isset($_POST['forgetpassword'])) {
    header("location: index.php");
}
else{ 

    $validator = new GUMP();

    // We get $_POST['email'] and $_POST['hash'] from the hidden input field of reset.php form
    $email = mysqli_real_escape_string($connection, $_SESSION['email']);
    $verification_code = mysqli_real_escape_string($connection, $_SESSION['verification_code']);
    $new_password = mysqli_real_escape_string($connection, $_POST['newpassword']);
    $conf_newpassword = mysqli_real_escape_string($connection, $_POST['conf_newpassword']);

    $_POST = array(
        'newpassword'   => $new_password,
        'conf_newpassword' 	  => $conf_newpassword
    );

    $_POST = $validator->sanitize($_POST);

    $rules = array(
        'newpassword'   => 'required|min_len,6|max_len,20',
        'conf_newpassword' 	  => 'required|min_len,6|max_len,20'
    );

    $filters = array(
        'newpassword'   => 'trim|sanitize_string',
        'conf_newpassword' 	  => 'trim|sanitize_string'
    );

    $_POST = $validator->filter($_POST, $filters);


    $validated = $validator->validate(
        $_POST, $rules
    );

    if($validated === TRUE){

        // Make sure the two passwords match
        if ( $new_password == $conf_newpassword ) { 

            $password_temp = SALT1."$new_password".SALT2;
            $password_hash = MD5($password_temp);
            
            $sql = "UPDATE users SET password='$password_hash', verification_code='' WHERE email='$email'";

                $result = mysqli_query($connection,$sql) or die(mysqli_error($connection));

                $_SESSION['message'] = "<div class=\"alert alert-success\" role=\"alert\">
                <h4 class=\"alert-heading\">Success!</h4>
                <p>Your password is reset!  </p>

                <p class=\"mb-0\">Please Log in!</p>

                </div>";
                header("location: index.php?#pagemessage.php");
        }
        else {
            $_SESSION['message'] = "<div class=\"alert alert-danger\" role=\"alert\">
            <h4 class=\"alert-heading\">Error!</h4>
            <p>The passwords you entered don't match. </p>

            <p class=\"mb-0\">Try again!</p>

            </div>";
            header("location: index.php?#pagemessage.php"); 
        }
    }
    else{
        echo $validator->get_readable_errors(true);
    }
}
?>