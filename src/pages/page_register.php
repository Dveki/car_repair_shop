<form class="formvalidate" id="formsubmit" method="post" name="register" action="register.php">

    <label for="firstname">First Name</label>
    </br>
    <input type="text" name="firstname" id="firstname">
    </br>

    <label for="lastname">Last Name</label>
    </br>
    <input type="text" name="lastname" id="lastname">
    </br>

    <label for="phonenum">Telephone Number </br> (+381-xx-xxxx-xxx(x))</label>
    </br>
    <input type="tel" name="phonenum" id="phonenum" placeholder="+381-xx-xxxx-xxx(x)">
    </br>

    <label for="email">Email</label>
    </br>
    <input type="email" name="email" id="email">
    </br>

    <label for="conf_email">Confirm Your Email</label>
    </br>
    <input type="email" name="conf_email" id="conf_email"> </br>

    <label for="password">Password</label>
    </br>
    <input type="password" name="password" id="password">
    </br>

    <label for="conf_password">Confirm Your Password</label>
    </br>
    <input type="password" name="conf_password" id="conf_password">
    </br>
    </br>
    <div id="captcha_element"></div>
    </br>
    <button type="submit" class="btn" id="mail-submit" name="register">Submit</button>
    <button type="reset" class="btn" name="reset" value="cancel">RESET</button>
    <p class="form-message"></p>
</form>
</br>
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
        async defer>
</script>
<script>
//escaped the error  because the OP is using somewhere an synchronous XMLHttpRequests
var url = "js/validation_form.js";
$.getScript(url);
</script>
