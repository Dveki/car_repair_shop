<?php

    echo <<<EOT
    <div class="container section-ourTeam">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 ourTeam-heading text-center">
          <h1>Meet Our Team</h1>
        </div>
      </div>
      <div id="team-holder" class="row">
      <div class="row team-holder">
        <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="row section-success ourTeam-box text-center">
            <div class="col-md-12 section1">
              <img src="img/team1.png">
            </div>
            <div class="col-md-12 section2">
              <p>PERA</p><br>
              <h1>OWNER</h1><br>
            </div>
            <div class="col-md-12 section3">
              <p>
                The big boss, the one who makes things happen, and the one who keeps the gears in motion. Pera is the face behind the workshop, the man who strives to make everything 
                work as it is supposed to.
              </p>
            </div>
            <div class="col-md-12 section4">
              <i class="fa fa-facebook-official" aria-hidden="true"></i>
              <i class="fa fa-twitter" aria-hidden="true"></i>
              <i class="fa fa-google-plus" aria-hidden="true"></i>
              <i class="fa fa-envelope" aria-hidden="true"></i>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-11">
          <div class="row section-info ourTeam-box text-center">
            <div class="col-md-12 section1">
              <img src="img/team2.png">
            </div>
            <div class="col-md-12 section2">
              <p>JOVAN</p><br>
              <h1>MECHANIC</h1><br>
            </div>
            <div class="col-md-12 section3">
              <p>
                Jovan is a man born with a wrench in one hand and a toy car in the other hand. He is a proper car guru, and a coffee connoisseur.
                Nothing is impossible, and everything is doable when Jovan is on the case.
              </p>
            </div>
            <div class="col-md-12 section4">
              <i class="fa fa-facebook-official" aria-hidden="true"></i>
              <i class="fa fa-twitter" aria-hidden="true"></i>
              <i class="fa fa-google-plus" aria-hidden="true"></i>
              <i class="fa fa-envelope" aria-hidden="true"></i>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="row section-danger ourTeam-box text-center">
            <div class="col-md-12 section1">
              <img src="img/team3.png">
            </div>
            <div class="col-md-12 section2">
              <p>JASNA</p><br>
              <h1>MARKETING/PR</h1>
            </div>
            <div class="col-md-12 section3">
              <p>
                Jasna makes sure to present the team in the best light, and she works hard to showcase the quality of our services.
                She is a boss in disguise, since every decision has to go by her before being realized.
              </p>
            </div>
            <div class="col-md-12 section4">
              <i class="fa fa-facebook-official" aria-hidden="true"></i>
              <i class="fa fa-twitter" aria-hidden="true"></i>
              <i class="fa fa-google-plus" aria-hidden="true"></i>
              <i class="fa fa-envelope" aria-hidden="true"></i>
            </div>
          </div>
        </div>
      </div>
      </div>
    </div>
  </div>
EOT;
?>
  