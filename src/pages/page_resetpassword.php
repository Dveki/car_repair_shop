<?php 
session_start();
// User needs to follow the link sent by email to see this page
if (!isset($_SESSION['email']) && !isset($_SESSION['verification_code']))
{
    $_SESSION['message'] = "<div class=\"alert alert-warning\" role=\"alert\">
    <h4 class=\"alert-heading\">Error!</h4>
    <p>You are not supposed to be here!</p>
    
    <p class=\"mb-0\">If you want to change your password, then click on Login, and then Forgot Password. </p>
    
    </div>";
    header("location: page_message.php");
}

?>
<form class="formvalidate" id="formreset" method="post" name="register" action="reset_password.php">

<h1>Choose Your New Password</h1>
    <label for="newpassword">New Password</label></br>
    <input type="password" name="newpassword" id="newpassword"></br>

    <label for="conf_newpassword">Confirm New Password</label></br>
    <input type="password" name="conf_newpassword" id="conf_newpassword"></br></br>

    <!-- This input field is needed, to get the email of the user -->
      
<button type="submit" id="forgetpassword" name="forgetpassword">Submit</button>
<button type="reset" name="reset" value="cancel">RESET</button>
<p class="form-message"></p>
</form>

<script>
//escaped the error  because the OP is using somewhere an synchronous XMLHttpRequests
var url = "js/validation_form.js";
$.getScript(url);
</script>