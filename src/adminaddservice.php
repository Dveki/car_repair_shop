<?php
require("include/config.php");
require("include/db.php");
require("include/gump.class.php");
if(isset($_POST["service_name"]) && isset($_POST["service_description"]) && isset($_POST["service_time"]) && isset($_POST["service_price"])) {

$validator = new GUMP();

$service_name = mysqli_real_escape_string($connection,$_POST["service_name"]);
$service_description =mysqli_real_escape_string($connection,$_POST["service_description"]);
$service_time = mysqli_real_escape_string($connection,$_POST["service_time"]);
$service_price =mysqli_real_escape_string($connection,$_POST["service_price"]);

$_POST = array(
  'servicename'   => $service_name,
  'servicedesc' 	  => $service_description,
  'servicetime'	      => $service_time,
  'serviceprice'    => $service_price,
);

$_POST = $validator->sanitize($_POST);

$rules = array(
  'servicename'   => 'required|alpha_space|min_len,3|max_len,64',
  'servicedesc' 	  => 'required|alpha_space',
  'servicetime'	      => 'required',
  'serviceprice'    => 'required',
);

$filters = array(
  'servicename'   => 'trim|sanitize_string',
  'servicedesc' 	  => 'trim|sanitize_string',
  'servicetime'	      => 'trim|sanitize_string',
  'serviceprice'    => 'trim|sanitize_string',
);

$_POST = $validator->filter($_POST, $filters);

$validated = $validator->validate(
    $_POST, $rules
);

if($validated === TRUE){

  if(isset($_FILES["file"]) AND is_uploaded_file($_FILES['file']['tmp_name'])) {

    $file_name = $_FILES['file']["name"];
    $file_temp = $_FILES["file"]["tmp_name"];
    $file_size = $_FILES["file"]["size"]; 
    $file_type = $_FILES["file"]["type"];
    $file_error = $_FILES['file']["error"];
  
    if ($file_error >0) {
      echo "Something went wrong during file upload!";
    }
    else {
  
      if (!exif_imagetype($file_temp)) {
        exit("File is not a picture!");
      }
  
      $ext_temp = explode(".", $file_name);
      $extension = end($ext_temp);
  
      $new_file_name = $ext_temp[0] . ".$extension";
      // 20171110084338.jpg
      $directory = "img/services";
  
      $upload = "$directory/$new_file_name"; // images/20171110084338.jpg
  
      // file upload
      if (!is_dir($directory)) {
        mkdir($directory);
      }
  
      if (move_uploaded_file($file_temp, $upload)) {
  
      }
      else {
        echo "<p><b>Error!</b></p>";
      }
  
    }
  
  }
     
$sql = "INSERT INTO services (service_name, service_description, service_time, service_price, service_image) VALUES ('$service_name', '$service_description', '$service_time', '$service_price','$new_file_name')";

$result = mysqli_query($connection,$sql) or die(mysqli_error($connection));

  echo "You have successfully added a new service.";
  exit();

mysql_close();

}
else{
  echo $validator->get_readable_errors(true);
}
}
else{
  header("location: index.php");
  exit();
}
?>