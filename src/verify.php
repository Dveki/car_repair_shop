<?php 
/* 
Verifies registered user email, the link to this page
is included in the register.php email message 
*/
session_start();
require("include/config.php");
require("include/db.php");
require("include/gump.class.php");

// Make sure email and hash variables aren't empty
if(isset($_GET['email']) && !empty($_GET['email']) && isset($_GET['verification_code']) && !empty($_GET['verification_code'])){

        $validator = new GUMP();

        $email = mysqli_real_escape_string($connection, $_GET['email']);
        $verification_code = mysqli_real_escape_string($connection, $_GET['verification_code']);
        
        $_POST = array(
                'email'	      => $email,
                'verificode'    => $verification_code
        );

        $_POST = $validator->sanitize($_POST);

        $rules = array(
                'email'	      => 'required|valid_email|min_len,3|max_len,32',
                'verificode'    => 'required|alpha_numeric'
        );

        $filters = array(
                'email'	      => 'trim|sanitize_string',
                'verificode'    => 'trim|sanitize_string'
        );

        $_POST = $validator->filter($_POST, $filters);


        $validated = $validator->validate(
                $_POST, $rules
        );


        var_dump($_POST);
        var_dump($validated);


        if($validated === TRUE){
    
    // Select user with matching email and verification_code, who hasn't verified their account yet (active = 0)
   $sql = "SELECT * FROM users WHERE email='$email' AND verification_code='$verification_code' AND verification_code !='' AND active='0'";
   
   
   $result = mysqli_query($connection,$sql) or die(mysqli_error($connection));
   

   if ($result->num_rows > 0) {

        while($row = $result->fetch_assoc()) {

        $verification_time = $row['verification_time'];

        }
        //checking if 24 hours for activation has expired
        if ($verification_time < date("Y-m-d H:i:s"))

        { $_SESSION['message'] = "<h1>Error</h1><br><p>Vreme od 24 sata za registraciju je isteklo<p>";
         header("location: index.php?#pagemessage.php");
       }
       //if time for verification has not expired then:
       else{
        
        // Set the user status to active (active = 1) and reset verification_code
        $sql_update = "UPDATE users SET active='1', verification_code='' WHERE email='$email'";

        $result = mysqli_query($connection,$sql_update) or die(mysqli_error($connection));

        $_SESSION['message'] = "<div class=\"alert alert-success\" role=\"alert\">
        <h4 class=\"alert-heading\">Success!</h4>
        <p>Your account has been activated!</p>

        <p class=\"mb-0\">Please Log in!</p>

        </div>";
         header("location: index.php?#pagemessage.php");
    }
}
//if user with email and status different from o already is in DB
elseif ( $result->num_rows == 0) {   
        $_SESSION['message'] = "<div class=\"alert alert-warning\" role=\"alert\">
        <h4 class=\"alert-heading\">Error!</h4>
        <p>Account has already been activated or the URL is invalid!</p>

        </div>";
        header("location: index.php?#pagemessage.php");
}


}
else{
        echo $validator->get_readable_errors(true);
    }  
}
//also direct URL protection
else {
        header("location: index.php");
} 
  
?>



